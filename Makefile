SHELL := bash -O extglob

.PHONY: render info tracks simulate burn cdemu rip clean

default: render


render/ddp/IMAGE.DAT: The\ Standard\ Model.RPP */*.RPP
	rm -rf render/ddp
	reaper -renderproject "The Standard Model.RPP"
	ddpinfo -y render/ddp

render/album.wav render/album.cue render/album.cdt: render/ddp/IMAGE.DAT
	ddpinfo -w render/album.wav render/ddp
render/album.cue: render/album.wav render/album.cdt
render: render/album.cue

info: render/ddp/IMAGE.DAT
	ddpinfo render/ddp

# render/mixbus.wav: render/album.cue render/album.wav render/album.cdt TSM-mixbus/TSM-mixbus.ardour
# 	$$(dirname $$(readlink -f $$(which Mixbus6)))/mixbus6-export TSM-mixbus TSM-mixbus
# 	mv TSM-mixbus/export/session.wav render/mixbus.wav
# render/mixbus.cue: render/mixbus.wav
# 	sed 's/album\.wav/mixbus.wav/; s/album\.cdt/mixbus.cdt/' render/album.cue > render/mixbus.cue
# 	cp render/album.cdt render/mixbus.cdt


render/tracks/wav: render/album.cue render/album.wav
	rm -rf render/tracks/wav
	mkdir -p render/tracks/wav
	shnsplit -d "render/tracks/wav" -f "render/album.cue" -o "wav" -t "%n %a - %p - %t" -- "render/album.wav"
	mv "render/tracks/wav/"*pregap.wav "render/tracks/wav/00 The Standard Model - pregap.wav"
	mv "render/tracks/wav/01"*.wav "render/tracks/wav/01 The Standard Model - PinkiePieSwear - Flutterwonder (SoGreatandPowerful Remix).wav"
render/tracks/flac: render/album.cue render/album.wav
	rm -rf render/tracks/flac
	mkdir -p render/tracks/flac
	shnsplit -d "render/tracks/flac" -f "render/album.cue" -o "flac flac -V --best --no-preserve-modtime -o %f -" -t "%n %a - %p - %t" -- "render/album.wav"
	mv "render/tracks/flac/"*pregap.flac "render/tracks/flac/00 The Standard Model - pregap.flac"
	mv "render/tracks/flac/01"*.flac "render/tracks/flac/01 The Standard Model - PinkiePieSwear - Flutterwonder (SoGreatandPowerful Remix).flac"
	cuetag.sh "render/album.cue" "render/tracks/flac/"!(*pregap).flac || cuetag "render/album.cue" "render/tracks/flac/"!(*pregap).flac
	metaflac --set-tag=ALBUMARTIST=SoGreatandPowerful --add-replay-gain "render/tracks/flac/"*.flac
	metaflac --list "render/tracks/flac/"*.flac | grep REPLAYGAIN_TRACK_GAIN.*
tracks: render/tracks/wav render/tracks/flac

render/album.flac: render/album.cue render/album.wav
	flac -V --best --tag=ALBUMARTIST=SoGreatandPowerful --replay-gain --tag-from-file=CUESHEET="render/album.cue" --cuesheet "render/album.cue" "render/album.wav" --no-preserve-modtime -f -o "render/album.flac"
# render/mixbus.flac: render/mixbus.cue render/mixbus.wav
# 	flac -V --best --tag=ALBUMARTIST=SoGreatandPowerful --replay-gain --tag-from-file=CUESHEET="render/mixbus.cue" --cuesheet "render/mixbus.cue" "render/mixbus.wav" --no-preserve-modtime -f -o "render/mixbus.flac"


simulate: render/album.cue
	@echo "Simulating burn...
	cd render && cdrecord -v -dummy -dao -eject -audio -text -useinfo textfile=album.cdt cuefile=album.cue

burn: render/album.cue
	@echo "Doing real burn of audio tracks... Press Ctrl+C to abort now. Burning starts in 5 seconds.
	@sleep 5
	cd render && cdrecord -v -dao -eject -audio -text -useinfo textfile=album.cdt cuefile=album.cue

render/data.iso: assets/data
	@echo Reading MSC from disc...
	cdrecord -msinfo > msc.txt
	@echo MSC: $$(< msc.txt)
	sleep 5
	rm -f render/data.iso
	xorriso -as mkisofs -v -f -C "$$(< msc.txt)" -J -r -V "The Standard Model" -o render/data.iso assets/data/

# cdemu/session1.toc: render/album.cue
# 	mkdir -p cdemu
# 	if [ -s cdemu/id ]; then cdemu unload $$(< cdemu/id); sleep 2; fi
# 	cdemu status | grep False | tail -n1 | awk '{print $$1}' > cdemu/id
# 	if [ ! -s cdemu/id ]; then cdemu add-device; sleep 3; cdemu status | grep False | tail -n1 | awk '{print $$1}' > cdemu/id; fi
# 	@echo "CDemu id:" $$(< cdemu/id)
# 	sleep 3
# 	rm -f cdemu/session1*
# 	cdemu tr-emulation $$(< cdemu/id) 1
# 	cdemu create-blank --writer-id=WRITER-TOC --medium-type=cdr80 $$(< cdemu/id) cdemu/session1.toc
# 	sleep 3
# 	cdemu device-mapping | grep '^'$$(< cdemu/id) | awk '{print $$2}' > cdemu/session1.dev
# 	(cd render && cdrecord dev=$$(< ../cdemu/session1.dev) speed=10 -v -dao -eject -audio -text -useinfo textfile=album.cdt cuefile=album.cue)
# 	sleep 3
# 	cdemu load $$(< cdemu/id) cdemu/session1.toc
# 	cd-info --no-cddb --no-device-info --no-header -C $$(< cdemu/session1.dev)
# 	cdemu unload $$(< cdemu/id)
# 	rm -f cdemu/id
# cdemu: cdemu/session1.toc

cdemu: render/album.cue
	mkdir -p cdemu
	if [ -s cdemu/id ]; then cdemu unload $$(< cdemu/id); sleep 2; fi
	cdemu status | grep False | tail -n1 | awk '{print $$1}' > cdemu/id
	if [ ! -s cdemu/id ]; then cdemu add-device; sleep 3; cdemu status | grep False | tail -n1 | awk '{print $$1}' > cdemu/id; fi
	@echo "CDemu id:" $$(< cdemu/id)
	sleep 1
	cdemu load $$(< cdemu/id) render/album.cue
	sleep 1
	cdemu device-mapping | grep '^'$$(< cdemu/id) | awk '{print $$2}' > cdemu/session1.dev
	sleep 1
	cd-info --no-cddb --no-device-info --no-header -C $$(< cdemu/session1.dev)

rip:
	mkdir -p rip
	(cd rip && cdrdao read-cd -v 2 --read-raw --session 1 --datafile session1.bin session1.toc && toc2cue session1.toc session1.cue)
	(cd rip && cdrdao read-cd -v 2 --read-raw --session 2 --datafile session2.bin session2.toc && toc2cue session2.toc session2.cue)
#	--driver generic-mmc:0x20000
#	flac --endian little --sign signed --channels 2 --bps 16 --sample-rate 44100 --cuesheet rip/session1.cue rip/session1.bin -o rip/session1.flac

clean:
#	if [ -s cdemu/id ]; then cdemu unload $$(< cdemu/id); fi
	rm -rf render cdemu TSM-mixbus/export rip
	find -type f -name '*.RPP-PROX' -delete -print
	find -type f -name '*.reapeaks' -delete -print
	find TSM-mixbus -type f -name '*.peak' -delete -print
