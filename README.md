The Standard Model CD Master
============================

Project for managing my master of SoGreatandPowerful's album The Standard Model. You need to provide the source music files to build a Red Book CD/DDP image. Reaper is used for this project. <!--An optional data session may be included on the disc after the audio session, which is a small archive of deleted descriptions, download links and comments.-->

![screenshot of reaper project](assets/images/screenshot_reaper.png "Screenshot of Reaper project")

Setup
-----

You need some music files, go to [media](media) for a list. Filenames have to match exactly.

You need [Reaper](https://www.reaper.fm/) to open and render the project, by default it is set up to generate a DDP image.

You don't have to use the command line, you can render the Reaper project manually. But if you want to automate the rendering, verifying, processing and splitting into tracks, keep reading. The Makefile expects the Reaper project to render a DDP image to `render/ddp/`.

Make sure `reaper` is in your `$PATH` by running `which reaper`. You should see one line of output.

You also need the following command line tools:

* GNU Make
* [DDP Mastering Tools](http://ddp.andreasruge.de/) [[download for Linux x86_64]](http://ddp.andreasruge.de/dist/ddptools-1.1-x86_64-elf.tar.gz) This is used to verify the DDP image created by Reaper and to convert it to cue+wav+cdt.

Some optional tools if you want to do more than just a CD image:
* flac
* shntool
* cuetag or cuetag.sh
* cdrecord (from cdrtools, to burn a cd)
* cdemu (for loading a virtual disc)
* cd-info (show info about a loaded disc)
* xorriso (for creating data session, wip)


Makefile commands
---------

Run `make` to render the DDP and as cue+wav+cdt. Files are output in the `render` directory. The DDP image is in `render/ddp`.

You can view information of the CDTEXT data by running `make info`. This runs ddpinfo on the DDP image. Long messages gets trimmed when viewed using this.

To render individual tracks, run `make tracks`. This will output to `render/tracks` in wav and flac formats (this requires flac and shntool).

You can create a virtual CD by running `make cdemu`. This will load the cue+wav combo with cdemu, so you can play it with a media player or inspect it with tools like cd-info.

To test the burning, run `make simulate` with a real CD burner and empty CD-R disc. It will do the burning process with the laser off. To do a real burn, run `make burn`. This will burn the audio tracks and close the disc. Multi session/enhanced CD support coming soon.
